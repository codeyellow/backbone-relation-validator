define(function (require) {
    'use strict';
    var _ = require('underscore');
    require('backbone-validator.js');
    var Validator = Backbone.Validator;

    var defaultOptions = {
        // Class where the error text will be appended to. If not found, the parent from the input is used.
        classErrorWrapper: '_error-wrapper',
        classHasError: 'has-error',
        classErrorText: 'error-text',
    };

    return function (options) {
        options = _.extend({}, defaultOptions, options);

        // Also validated nested models correctly.
        Validator.getAttrsToValidate = function(model, passedAttrs) {
            var modelAttrs = model.attributes;
            var attrs = {};

            if (_.isArray(passedAttrs)) {
                if (_.isString(passedAttrs)) {
                    passedAttrs = [passedAttrs];
                }

                _.each(passedAttrs, function(attr) {
                    attrs[attr] = model.dot(attr) || model.get(attr);
                });
            } else if (!passedAttrs) {
                var all = _.extend({}, modelAttrs, _.result(model, 'validation') || {});

                _.each(all, function(val, attr) {
                    attrs[attr] = model.dot(attr);
                });
            } else {
                attrs = passedAttrs;
            }

            return attrs;
        };


        // The element to bind the error to.
        // First, try to find a class to bind it to, if it can not be found,
        // fallback to the parent of the name attribute.
        function getEl(view, attr) {
            var nameEl = view.$('[name="' + attr + '"]');
            var closest = nameEl.closest('.' + options.classErrorWrapper, view);
            if (closest.length) {
                return closest;
            }
            return nameEl.parent();
        }

        Validator.ViewCallbacks = {
            // The input is valid, so remove the error that was appended.
            onValidField: function(attr) {
                var $el = getEl(this, attr);

                $el.removeClass(options.classHasError);
                $el.find('.' + options.classErrorText).remove();
            },
            // The input is invalid, so add an error message to it.
            onInvalidField: function(attr, value, errors) {
                var $el = getEl(this, attr);
                var $errorText = $el.find('.' + options.classErrorText);
                var errorList = errors.join(', ');

                $el.addClass(options.classHasError);

                if ($errorText.length) {
                    $errorText.html(errorList);
                } else {
                    $el.append('<div class="' + options.classErrorText + '">' + errorList + '</div>');
                }
            },
        };

        Validator.add('oneOf', function(value, expectation) {
            return _.include(expectation, value);
        }, 'Must be one of');
    };
});
