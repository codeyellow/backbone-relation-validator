define(function (require) {
    'use strict';
    var Validator = Backbone.Validator;
    var _ = require('underscore');

    function formatPlaceholders() {
        var args = Array.prototype.slice.call(arguments);
        var text = args.shift();
        return text.replace(/\{(\d+)\}/g, function(match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };

    var messages = {
        required: 'This value is required.',
        unique: 'This value is not unique.',
        exists: 'This value does not exist.',
        password: 'This password is not strong enough.',
        date: 'This value must be a valid date.',
        min: 'This value must contains at least {0} characters.',
        date_format: 'This value does not match the format {0}.',
        after: 'This value must be a date after {0}.',
        before: 'This value must be a date before {0}.',
        mimes: 'This file must be of type: {0}.',
        image: 'This file must be an image.',
    };

    function getMessage(modelOrView, code) {
        return _.result(modelOrView, 'errorMessages[' + code + ']', messages[code] || code);
    }

    // Translate the backend validation error to a nice message.
    // TODO: This should also return values such as the maxLength.
    function translateBackendError(modelOrView, val) {
        const errorObj = _.clone(val);
        const message = [getMessage(modelOrView, errorObj.code)];
        delete errorObj.code;

        for (var key in errorObj) {
            if (errorObj.hasOwnProperty(key)) {
                message.push(errorObj[key]);
            }
        }
        return formatPlaceholders.apply(this, message);
    }

    function convert(modelOrView, validationErrors) {
        var errors = {};
        _.each(validationErrors, function(valError, name) {
            errors[name] = _.map(valError, translateBackendError.bind(this, modelOrView));
        });
        return errors;
    }

    function getInputsFromView(view) {
        var attrs = [];
        if (view) {
            view.$('form [name]').each(function() {
                if (/^(?:input|select|textarea)$/i.test(this.nodeName) && this.name &&
                    this.type !== 'submit' && attrs.indexOf(this.name) === -1) {
                    attrs.push(this.name);
                }
            });
        }
        return attrs;
    }

    function clearErrorsFromView(view, callbackOptions) {
        var inputNames = getInputsFromView(view);

        inputNames.forEach(function(name) {
            callbackOptions.onValidField.call(view, name);
        });
    }

    function mapErrorsFromView(view, errors, callbackOptions) {
        _.each(errors, function(error, name) {
            callbackOptions.onInvalidField.call(view, name, '', error);
        });
    }

    return function(xhr, modelOrView) {
        var errors;

        if (
            xhr.responseJSON && xhr.responseJSON.error &&
            xhr.responseJSON.error.validation_errors
        ) {
            errors = convert(modelOrView, xhr.responseJSON.error.validation_errors);
        }

        // Render errors via model, the normal way.
        if (modelOrView instanceof Backbone.Model) {
            return modelOrView.triggerValidated(null, errors);
        }

        var callbackOptions = _.extend({}, Validator.ViewCallbacks, _.pick(modelOrView, 'onInvalidField', 'onValidField'));

        // Assume a view is given, so render validation errors the dirty way™.
        // This should only be used if it's not possible to use a model, because it doesn't
        // trigger the correct events like the 'normal way'.
        clearErrorsFromView(modelOrView, callbackOptions);

        if (errors) {
            mapErrorsFromView(modelOrView, errors, callbackOptions);
        }
    }
});
