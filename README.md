# backbone-relation-validator

This package enables you to validate nested models using [backbone-relation](https://github.com/CodeYellowBV/backbone-relation). It uses [backbone-validator](https://github.com/fantactuka/backbone-validator).

It also provides some sane defaults for backbone-validator, as it's quite sparse.

It can also show back end validation errors in the front end.

## Usage

```js
var validator = require('backbone-relation-validator');
validator({
    classErrorWrapper: '._plz-wrap-my-error', // optional
});
```

## Backend

The back end needs to follow our validation error standard and return 400. An example JSON response:

```json
{
    "error": {
        "code": "ParameterError",
        "validation_errors": {
            "username": [
                { "code": "unique" }
            ],
            "password": [
                { "code": "min", "min": "8" },
                { "code": "password" }
            ]
        }
    }
}
```

When adding an error message to an input, it first tries to search for `options.classErrorWrapper` (by default `._error-wrapper`) to append the error text to. If the class can not be found, the direct parent of the input will be used.

Mapping errors from the back end to the front end works on every XHR request (it bypasses backbone-validator requirement for a model). You can either give a model or view as second argument. Example:

```js
var validationBackend = require('backbone-validation-backend/convert-backend');

return Marionette.ItemView.extend({
    submit: function (e) {
        var xhr = this.model.save(null, {
            success: function () {
                validationBackend(xhr, this.model);
            }.bind(this),
            error: function () {
                validationBackend(xhr, this.model);
            }.bind(this),
        });
    }
});
```

Or:

```js
var xhr = $.ajax({
    type: 'POST',
    url: 'api/user_username_check',
});

xhr.always(function () {
    validationBackend(xhr, this);
}.bind(this));
```

Often the backend uses unknown error codes to backbone-validator. By adding a `errorMessages` object to the model or view, you can map these error codes to messages. This can be a string or function. An example:

```js
Marionette.ItemView.extend({
    errorMessages: {
        min: 'Min length must be {0} characters.',
    }
});
```
